'use strict';
var path = require('canonical-path');
var Dgeni = require('dgeni');

var Package = Dgeni.Package;
module.exports = new Package('app', [require('dgeni-packages/ngdoc')])
  .config(function(log, readFilesProcessor, templateFinder, writeFilesProcessor) {
    log.level = 'info';
    readFilesProcessor.basePath = path.resolve(__dirname);
    readFilesProcessor.sourceFiles = [
      {
        include: 'app/scripts/**/*.js',
        basePath: 'app/scripts'
      }
    ];
    writeFilesProcessor.outputFolder = 'docs';
  })
;
