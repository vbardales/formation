# GTD - exercices

Chaque exercice est dans une branche nommée `exercice0X`, où `X` est le numéro de l'exercice.

La branche `master` est l'endroit où on développe.

À la fin d'un exercice, mergez ou rebasez la branche de l'exercice suivant dans `master`.
Choisissez lors du merge de conserver votre travail ou de garder la solution officielle.

Les exercices apparaissent dans le répertoire `exercices`.
Comme ce *readme*, ils sont en Markdown. Utilisez un plugin approprié si vous voulez les voir jolis dans votre IDE (sous WebStorm, *MultiMarkdown* est super).

Chaque exercice va demander de coder dans des fichiers de l'application.
Les endroits où coder sont matérialisés par des `TODO` en commentaire.

Les exercices donnent des liens vers la documentation officielle d'Angular.

Il y a 6 exercices (branches `exercice01` à `exercice06`), plus un exercice « bonus » pour les plus rapides (branche `exerciceX`).
L'application complète est dans la branche `complete`.

Commençons ! Mergez la branche `exercice01` !
