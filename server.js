"use strict";

var express = require('express');
var bodyParser = require('body-parser');
var _ = require('lodash');

var app = express();

app.use('/', express.static(__dirname + '/app'));
app.use('/node_modules/angular', express.static(__dirname + '/node_modules/angular'));
app.use('/node_modules/angular-route', express.static(__dirname + '/node_modules/angular-route'));
app.use('/node_modules/angular-resource', express.static(__dirname + '/node_modules/angular-resource'));
app.use('/node_modules/bootstrap', express.static(__dirname + '/node_modules/bootstrap'));
app.use('/rest', bodyParser.json());
app.listen(9000);

var ids = 20;
var tasks = {
  1: {
    id: 1,
    title: 'Une tâche',
    plannedFor: new Date(Date.now() - 86400000),
    duration: 5, // minutes
    description: 'A more complete description of une tâche :p'
  },
  2: {
    id: 2,
    title: 'Une autre tâche',
    plannedFor: new Date(Date.now() + 86400000),
    duration: 1, // minutes
    description: "Description complète de l'autre tâche"
  },
  3: {
    id: 3,
    title: 'Tâche de remplissage',
    plannedFor: Date.now() + 86400000,
    duration: 60, // minutes
    description: "<p class='text-info'>T'as vu !</p>"
  },
  4: {
    id: 4,
    title: 'Tâche de remplissage',
    plannedFor: new Date(Date.now() + 86400000),
    duration: 60, // minutes
    description: "<p class='text-info'>T'as vu !</p>"
  },
  5: {
    id: 5,
    title: 'Tâche de remplissage',
    plannedFor: new Date(Date.now() + 86400000),
    duration: 60, // minutes
    description: "<p class='text-info'>T'as vu !</p>"
  },
  6: {
    id: 6,
    title: 'Tâche de remplissage',
    plannedFor: new Date(Date.now() + 86400000),
    duration: 60, // minutes
    description: "<p class='text-info'>T'as vu !</p>"
  },
  7: {
    id: 7,
    title: 'Tâche de remplissage',
    plannedFor: new Date(Date.now() + 86400000),
    duration: 60, // minutes
    description: "<p class='text-info'>T'as vu !</p>"
  },
  8: {
    id: 8,
    title: 'Tâche de remplissage',
    plannedFor: new Date(Date.now() + 86400000),
    duration: 60, // minutes
    description: "<p class='text-info'>T'as vu !</p>"
  },
  9: {
    id: 9,
    title: 'Tâche de remplissage',
    plannedFor: new Date(Date.now() + 86400000),
    duration: 60, // minutes
    description: "<p class='text-info'>T'as vu !</p>"
  }
};

app.get('/rest/tasks', function (req, res) {
  res.json(_.forEach(_.map(tasks), function (task) {
    var smallerTask = _.assign({}, task);
    delete smallerTask.description;
    return smallerTask;
  }));
});

app.get('/rest/tasks/:taskId', function (req, res) {

  if (tasks[req.params.taskId]) {
    res.json(tasks[req.params.taskId]);
  }
  else {
    res.status(404).send('No task with ID ' + req.params.taskId);
  }
});

app.post('/rest/tasks', function (req, res) {
  var task = req.body;
  task.id = ++ids;
  tasks[task.id] = task;
  res.json(task);
});

app.post('/rest/tasks/:taskId', function (req, res) {
  var task = req.body;

  if (req.params.taskId != task.id) {
    res.status(400).send('IDs do not match in URL and in sent task.');
  }

  tasks[task.id] = task;
  res.json(task);
});

app.delete('/rest/tasks/:taskId', function (req, res) {
  if (tasks[req.params.taskId]) {
    delete tasks[req.params.taskId];
    res.status(200).send('Deleted.');
  }
  else {
    res.status(200).send('Noting matches, doing nothing.');
  }
});

app.get('/rest/check/date/isDayOpened', function(req, res) {
  // Fake server lag, 3 seconds
  setTimeout(function() {
    var day = new Date(req.query.date).getDay();
    if (day > 0 && day < 6) {
      res.status(200).json({
        day: day
      });
    }
    else {
      res.status(400).json({
        day: day
      });
    }
  }, 3000);
});
