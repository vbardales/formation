'use strict';

angular.module('app')
  .filter('days', [
    function() {
      return function(nbDays) {
        var date = new Date();
        var millis = date.setUTCHours(0, 0, 0, 0);
        date.setTime(millis + (nbDays * 1000 * 60 * 60 * 24));
        return date;
      }
    }
  ])
  .directive('gtdDayIsFuture', [
    function() {
      /**
       * @ngdoc directive
       * @name gtdDayIsFuture
       * @module app
       * @require ngModel
       * @restrict A
       * @description
       * Verifies that the date in the field is in the future.
       *
       * This is attached to `ngModel`'s `$validators` dictionary.
       * The validation key to check is `future`.
       */
      return {
        // TODO implémenter la directive pour qu'elle fonctionne comme décrit dans la doc (ex6)
      };
    }
  ])
  .directive('gtdDayIsOpened', [
    function() {
      /**
       * @ngdoc directive
       * @name gtdDayIsOpened
       * @module app
       * @require ngModel
       * @restrict A
       * @description
       * Verifies that the date in the field is an opened day, e.g. not a week-end, not a hollyday, ...
       *
       * The actual behavior is implemented server-side,
       * since it is very difficult to get a comprehensive and business-accurate representation of an “opened day” on the client.
       *
       * The server must respond to a GET request on `rest/check/date/isDayOpened` with the `date` query parameter,
       * and answer with:
       * - a 200 OK answer if the date is opened
       * - a 400 BAD PARAMETER answer is not
       *
       * This is attached to `ngModel`'s `$asyncValidators` dictionary.
       * The validation key to check is `dayIsOpened`.
       *
       * *note for exercise 6, the server actually only implements week-ends.*
       */
      return {
        // TODO implémenter la directive pour qu'elle fonctionne comme décrit dans la doc (ex6)
      };
    }
  ])
;
