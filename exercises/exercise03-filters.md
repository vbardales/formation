# Exercice 3 : Filtrer (démarrer de la branche `exercice03`)

## Utilisation de filtres

1. [Ordonner](https://code.angularjs.org/1.4.7/docs/api/ng/filter/orderBy) les tâches par date planifiée ascendante (la planification la plus proche d'abord) dans *home/home.html*
1. [Limiter](https://code.angularjs.org/1.4.7/docs/api/ng/filter/limitTo) le nombre de tâches affichées et afficher le lien quand on est limité.
1. Supprimer la limite lors d'un clic sur le lien, cacher le lien une fois qu'on a cliqué dessus.

## Création de filtres

1. Créer un filtre `isLate` dans *common/statusFilters.js*. Ce filtre accepte les tâches qui sont en retard.
1. Créer un filtre `isShort` dans *common/statusFilters.js*. Ce filtre accepte les tâches qui durent moins de 2 minutes.
1. Lancer un npm test. Les tests des filtres devraient passer sans problème.
1. N'afficher que les tâches qui ne sont ni courtes, ni en retard dans *home/home.html*
    - Dans `HomeController`, [injecter les filtres](https://code.angularjs.org/1.4.7/docs/guide/filter#using-filters-in-controllers-services-and-directives)
    - Créer une méthode pour filtrer la liste de tâches
    - Pourquoi ça ne marche pas ?
    - Filtrer les tâches lors du chargement
1. Ajouter deux listes sur le modèle de la première dans *home/home.html*
    - Une liste de tâches en retard. Remplacer la classe `panel-default` dans l'en-tête par `panel-danger` et le titre par "Tâches en retard"
    - Une liste de tâches courtes. Remplacer la classe `panel-default` dans l'en-tête par `panel-warning` et le titre par "Tâches courtes"
    - Dans la liste existante, n'afficher que les tâches qui ne sont ni en retard, ni courtes. Remplacer le titre par "Autres tâches".
    - Comment gérer les limites d'affichage indépendement ?


Et là, c'est le drame, on a 3 fois le même code, à quelques micro-details près… Mergez `exercice04` !
