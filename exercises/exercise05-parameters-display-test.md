# Exercice 04 : Paramètres, affichage et tests (démarrer de la branche `exercice05`)

## Paramètres de service et de route

1. Paramétriser la route dans *task/task.js*
2. [Injecter le paramètre](https://code.angularjs.org/1.4.7/docs/api/ngRoute/service/$routeParams) dans le contrôleur et faire l'appel au service.
3. Ajouter le code ci-dessous dans *task/task.spec.js* à l'emplacement indiqué dans le fichier. Lancer `npm test`. Le test doit passer !

<pre>
$httpBackend.expect('GET', 'http://localhost:9000/rest/tasks/12').respond(task);

expect(scope.task).toBeDefined();
expect(scope.task.id).toBeUndefined();

$httpBackend.flush();

expect(scope.task.id).toBe(12);
</pre>

## Affichage

Les tâches ont un certain nombre de choses à afficher de manières différentes

### Durée

La durée de la tâche est en minutes. C'est illisible pour un humain normal. Copier le code ci-dessous dans *common/statusFilters.js*

<pre>
.filter('remaining', [
        function () {
            return function (totalMinutes) {
                var components = [];
                var minutes = totalMinutes % 60;
                components.unshift('' + minutes + 'm');
                var totalHours = (totalMinutes - minutes) / 60;
                if (totalHours > 0) {
                    var hours = totalHours % 24;
                    components.unshift('' + hours + 'h');
                    var days = (totalHours - hours) / 24;
                    if (days > 0) {
                        components.unshift('' + days + 'd');
                    }
                }
                return components.join(' ');
            };
        }
    ])
</pre>

1. Écrire un test dans *common/statusFilters.spec.js* qui vérifie les points suivants:
    - N'affiche pas d'heures s'il y a moins de 60 minutes
    - Affiche les heures s'il y a plus de 60 minutes
    - Affiche le bon nombre de minutes après les heures
2. Ajouter un test qui vérifie qu'on n'affiche pas les minutes s'il en reste 0 après avoir affiché les heures. Le faire passer.

## Afficher du HTML

S'il y a du HTML dans la description, elle ne s'affiche pas. C'est dû à `ngBindHtml` qui vérifie que le HTML passé ne contient pas de trous de sécurité (XSS, images espion/CSS, …).
Il y a 2 solutions possibles:

- Utiliser le module [ngSanitize](https://code.angularjs.org/1.4.7/docs/api/ngSanitize) qui va s'assurer que le HTML soit propre. ngSanitize est très strict, donc le HTML en entrée doit être bien formé.
- Considérer que le HTML d'une description de tâche provient d'une source de confiance et l'autoriser explicitement

C'est cette dernière qu'on va mettre en place.

1. Créer un filtre `description` dans *common/statusFilters.js*. Il prend une tâche en paramètre et retourne sa description en HTML. L'appliquer dans *task/task.html*.
1. Ça ne suffit pas: il faut explicitement autoriser ce HTML à être affiché. Injecter `$sce` dans le filtre et [autoriser le HTML](https://code.angularjs.org/1.4.7/docs/api/ng/service/$sce#trustAsHtml) de la description.


Maintenant qu'on a une tâche, il est temps de pouvoir la modifier ! Mergez `exercice06` !
